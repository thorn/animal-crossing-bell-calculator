Animal Crossing Bell Calculator
================================================================================

This is a way to see how many bells you will recieve from Timmy/Tommy in Nook's
Cranny in the game Animal Crossing: New Horizons.


Building:
================================================================================

Just run the standard `go build main.go`. ACBC uses packages from a default go
install.
