/*
Animal Crossing Bell Calculator

This is a tool that allows you to see how many bells you will recieve from
Nook's Cranny in Animal Crossing: New Horizons
*/

/*
******FUCKING WHATEVER LICENSE (2021)******
This license allows you to do whatever the FUCK you want to do with it
Want to use it for good? Go the FUCK ahead!
Want to use it for bad? Go the FUCK ahead!

Just know that I, DONNIE CORBITT [wy] am not responsible for YOUR
fuckups by using this code. If your computer starts killing everyone in your
neighborhood it's your own damn fault!
*/

package main

import (
    "fmt"
)

func main() {
    fmt.Println("What are you selling?"+
    "\n1) Bugs"+
    "\n2) Fish")

    var BugOrFish string

    fmt.Scanln(&BugOrFish)
    if *&BugOrFish == "1" || *&BugOrFish == "Bugs" {
        // This scans to see what bug you want to sell from A-Z, since there are
        // 80 bugs, we don't want to print them all at once.
        fmt.Println("What letter does the bug start with? (A-Z)")
        var BugLetter string
        fmt.Scanln(&BugLetter)
        fmt.Println("Bugs that start with the letter " + *&BugLetter +":")

        // If [letter] == [letter], print out all bugs that start with said 
        // letter
        // TODO: Make letter choise be case insensitive. (FIXED!)
        if *&BugLetter == "A" || *&BugLetter == "a" {
            fmt.Println("\n1) Agrais Butterfly"+
             "\n2) Ant"+
             "\n3) Atlas Moth")
        } else if *&BugLetter == "B" || *&BugLetter == "b" {
            fmt.Println("\n4) Bagworm"+
            "\n5) Banded Dragonfy"+
            "\n6) Bell Cricket"+
            "\n7) Blue Weevil Beetle"+
            "\n8) Brown Cicada")
        } else if *&BugLetter == "C" || *&BugLetter == "C" {
            fmt.Println("\n9) Centipede"+
            "\n10) Cicada Shell"+
            "\n11) Citrus Long-Horned Beetle"+
            "\n12) Common Bluebottle"+
            "\n13) Common Butterfly"+
            "\n14) Cricket"+
            "\n15) Cyclommatus Stag")
        } else if *&BugLetter == "D" || *&BugLetter == "d" {
            fmt.Println("\n16) Damselfly"+
            "\n17) Darner Dragonfly"+
            "\n18) Diving Beetle"+
            "\n19) Drone Beetle"+
            "\n20) Dung Beetle")
        } else if *&BugLetter == "E" || *&BugLetter == "e" {
            fmt.Println("\n21) Earth-Boring Dung Beetle"+
            "\n22) Emperor Butterfly"+
            "\n23) Evening Cicada")
        } else if *&BugLetter == "F" || *&BugLetter == "f" {
            fmt.Println("\n24) Firefly"+
            "\n25) Flea"+
            "\n26) Fly")
        } else if *&BugLetter == "G" || *&BugLetter == "g" {
            fmt.Println("\n27) Giant Cicada"+
            "\n28) Giant Stag"+
            "\n29) Giant Water Bug"+
            "\n30) Giraffe Stag"+
            "\n31) Golden Stag"+
            "\n32) Goliath Beetle"+
            "\n33) Grasshopper"+
            "\n34) Great Purple Emperor")
        } else if *&BugLetter == "H" || *&BugLetter == "h" {
            fmt.Println("\n35) Hermit Crab"+
            "\n36) Honeybee"+
            "\n37) Horned Atlas"+
            "\n38) Horned Dynastid"+
            "\n39) Horned Elephant"+
            "\n40) Horned Hercules")
        } else if *&BugLetter == "J" || *&BugLetter == "j" {
            fmt.Println("\n41) Jewel Beetle")
        } else if *&BugLetter == "L" || *&BugLetter == "l" {
            fmt.Println("\n42) Ladybug"+
            "\n43) Long Locust")
        } else if *&BugLetter == "M" || *&BugLetter == "m" {
            fmt.Println("\n44) Madagascan Sunset Moth"+
            "\n45) Man-Faced Stink Bug"+
            "\n46) Mantis"+
            "\n47) Migratory Locust"+
            "\n48) Miyama Stag"+
            "\n49) Mole Cricket"+
            "\n50) Monarch Butterfly"+
            "\n51) Mosquito"+
            "\n52) Moth")
        } else if *&BugLetter == "O" || *&BugLetter == "o" {
            fmt.Println("\n53) Orchid Mantis")
        } else if *&BugLetter == "P" || *&BugLetter == "p" {
            fmt.Println("\n54) Paper Kite Butterfly"+
            "\n55) Peacock Butterfly"+
            "\n56) Pill Bug"+
            "\n57) Pondskater")
        } else if *&BugLetter == "Q" || *&BugLetter == "q" {
            fmt.Println("\n58) Queen Alexandra's Birdwing")
        } else if *&BugLetter == "R" || *&BugLetter == "r" {
            fmt.Println("\n59) Rainbow Stag"+
            "\n60) Rajah Brooke's Birdwing"+
            "\n61) Red Dragonfly"+
            "\n62) Rice Grasshopper"+
            "\n63) Robust Cicada"+
            "\n64) Rosalia Batesi Beetle")
        } else if *&BugLetter == "S" || *&BugLetter == "s" {
            fmt.Println("\n65) Saw Stag"+
            "\n66) Scarab Beetle"+
            "\n67) Scorpion"+
            "\n68) Snail"+
            "\n69) Spider"+ // nice
            "\n70) Stinkbug")
        } else if *&BugLetter == "T" || *&BugLetter == "t" {
            fmt.Println("\n71) Tarantula"+
            "\n72) Tiger Beetle"+
            "\n73) Tiger Butterfly")
        } else if *&BugLetter == "V" || *&BugLetter == "v" {
            fmt.Println("\n74) Violin Beetle")
        } else if *&BugLetter == "W" || *&BugLetter == "w" {
            fmt.Println("\n75) Walker Cicada"+
            "\n76) Walking Leaf"+
            "\n77) Walking Stick"+
            "\n78) Wasp"+
            "\n79) Wharf Roach")
        } else if *&BugLetter == "Y" || *&BugLetter == "y" {
            fmt.Println("\n80) Yellow Butterfly")
        } else {
            fmt.Println("There is no bug that starts with the letter "+ 
            *&BugLetter)
        }
        
        fmt.Println("\n\nWhat bug are you selling?")
        var BugSell string
        fmt.Scanln(&BugSell)
        fmt.Println("You are selling a " + *&BugSell)
    } else if *&BugOrFish == "2" || *&BugOrFish == "Fish" {
        fmt.Println("What letter does the fish start with? (A-Z)")
        var FishLetter string
        fmt.Scanln(&FishLetter)
        fmt.Println("Fish that start with the letter " + *&FishLetter +":")
        
        if *&FishLetter == "A" || *&FishLetter == "a" {
            fmt.Println("\n1) Anchovy"+
            "\n2) Angelfish"+
            "\n3) Arapaima"+
            "\n4) Arowana")
        } else if *&FishLetter == "B" || *&FishLetter == "b" {
            fmt.Println("\n5) Barred Knifejaw"+
            "\n6) Barreleye"+
            "\n7) Betta"+
            "\n8) Bitterling"+
            "\n9) Black Bass"+
            "\n10) Blowfish"+
            "\n11) Bluegill"+
            "\n12) Blue Marlin"+
            "\n13) Butterfly Fish")
        } else if *&FishLetter == "C" || *&FishLetter == "c" {
            fmt.Println("\n14) Carp"+
            "\n15) Catfish"+
            "\n16) Char"+
            "\n17) Cherry Salmon"+
            "\n18) Clown Fish"+
            "\n19) Coelacanth"+
            "\n20) Crawfish"+
            "\n21) Crucian Carp")
        } else if *&FishLetter == "D" || *&FishLetter == "d" {
            fmt.Println("\n22) Dab"+
            "\n23) Dace"+
            "\n24) Dorado")
        } else if *&FishLetter == "F" || *&FishLetter == "f" {
            fmt.Println("\n25) Football Fish"+
            "\n26) Freshwater Goby"+
            "\n27) Frog")
        } else if *&FishLetter == "G" || *&FishLetter == "g" {
            fmt.Println("\n28) Gar"+
            "\n29) Giant Snakehead"+
            "\n30) Giant Trevally"+
            "\n31) Golden Trout"+
            "\n32) Goldfish"+
            "\n33) Great White Shark"+
            "\n34) Guppy")
        } else if *&FishLetter == "H" || *&FishLetter == "h" {
            fmt.Println("\n35) Hammerhead Shark"+
            "\n36) Horse Mackerel")
        } else if *&FishLetter == "K" || *&FishLetter == "k" {
            fmt.Println("\n37) Killifish"+
            "\n38) King Salmon"+
            "\n39) Koi")
        } else if *&FishLetter == "L" || *&FishLetter == "l" {
            fmt.Println("\n40) Loach")
        } else if *&FishLetter == "M" || *&FishLetter == "m" {
            fmt.Println("\n41) Mahi-Mahi"+
            "\n42) Mitten Crab"+
            "\n43) Moray Eel")
        } else if *&FishLetter == "N" || *&FishLetter == "n" {
            fmt.Println("\n44) Napoleonfish"+
            "\n45) Neon Tetra"+
            "\n46) Nibble Fish")
        } else if *&FishLetter == "O" || *&FishLetter == "o" {
            fmt.Println("\n47) Oarfish"+
            "\n48) Ocean Sunfish"+
            "\n49) Olive Flounder")
        } else if *&FishLetter == "P" || *&FishLetter == "p" {
            fmt.Println("\n50) Pale Chub"+
            "\n51) Pike"+
            "\n52) Piranha"+
            "\n53) Pond Smelt"+
            "\n54) Pop-Eyed Goldfish"+
            "\n55) Puffer Fish")
        } else if *&FishLetter == "R" || *&FishLetter == "r" {
            fmt.Println("\n56) Rainbowfish"+
            "\n57) Ranchu Goldfish"+
            "\n58) Ray"+
            "\n59) Red Snapper"+
            "\n60) Ribbon Eel")
        } else if *&FishLetter == "S" || *&FishLetter == "s" {
            fmt.Println("\n61) Saddled Bichir"+
            "\n62) Salmon"+
            "\n63) Saw Shark"+
            "\n64) Sea bass"+ // IT'S AT LEAST A C+ HAHAHAHAHAHAHAHAHAHA
            "\n65) Sea Butterfly"+
            "\n66) Sea Horse"+
            "\n67) Snapping Turtle"+
            "\n68) Soft-Shelled Turtle"+
            "\n69) Squid"+ // 127:29
            "\n70) Stringfish"+
            "\n71) Sturgeon"+
            "\n72) Suckerfish"+
            "\n73) Surgeonfish"+
            "\n74) Sweetfish")
        } else if *&FishLetter == "T" || *&FishLetter == "t" {
            fmt.Println("\n75) Tadpole"+
            "\n76) Tilapia"+
            "\n77) Tuna")
        } else if *&FishLetter == "W" || *&FishLetter == "w" {
            fmt.Println("\n78) Whale Shark")
        } else if *&FishLetter == "Y" || *&FishLetter == "y" {
            fmt.Println("\n79) Yellow Perch")
        } else if *&FishLetter == "Z" || *&FishLetter == "z" {
            fmt.Println("\n80) Zebra Turkeyfish")
        }
        
        fmt.Println("\n\nWhat fish are you selling?")
        var FishSell string
        fmt.Scanln(&FishSell)
        fmt.Println("You are selling a " + *&FishSell)
    }
}
